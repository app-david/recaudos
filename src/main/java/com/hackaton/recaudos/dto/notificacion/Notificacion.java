package com.hackaton.recaudos.dto.notificacion;

import com.hackaton.recaudos.dto.TransaccionPago;
import com.hackaton.recaudos.dto.transaccion.Transaccion;

public class Notificacion {

    private String codigoConvenio;
    private Transaccion transaccionPago;

    public String getCodigoConvenio() {
        return codigoConvenio;
    }

    public void setCodigoConvenio(String codigoConvenio) {
        this.codigoConvenio = codigoConvenio;
    }

    public Transaccion getTransaccionPago() {
        return transaccionPago;
    }

    public void setTransaccionPago(Transaccion transaccionPago) {
        this.transaccionPago = transaccionPago;
    }
}
