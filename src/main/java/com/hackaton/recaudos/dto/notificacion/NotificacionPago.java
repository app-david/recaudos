package com.hackaton.recaudos.dto.notificacion;

import com.hackaton.recaudos.dto.transaccion.RecaudosRq;

public class NotificacionPago {

    private RecaudosRq recaudosRq;

    public RecaudosRq getRecaudosRq() {
        return recaudosRq;
    }

    public void setRecaudosRq(RecaudosRq recaudosRq) {
        this.recaudosRq = recaudosRq;
    }
}
