package com.hackaton.recaudos.dto;

import com.hackaton.recaudos.dto.notificacion.NotificacionPago;

public class NotificacionRequest {

    private NotificacionPago notificarPago;

    public NotificacionPago getNotificarPago() {
        return notificarPago;
    }

    public void setNotificarPago(NotificacionPago notificarPago) {
        this.notificarPago = notificarPago;
    }
}
