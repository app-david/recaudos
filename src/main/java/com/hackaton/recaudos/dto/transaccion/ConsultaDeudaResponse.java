package com.hackaton.recaudos.dto.transaccion;

public class ConsultaDeudaResponse {

    private RecaudosRq recaudosRs;

    public RecaudosRq getRecaudosRs() {
        return recaudosRs;
    }

    public void setRecaudosRs(RecaudosRq recaudosRq) {
        this.recaudosRs = recaudosRq;
    }
}
