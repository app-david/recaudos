package com.hackaton.recaudos.dto.transaccion;

public class Cabecera {

    private Operacion operacion;

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }
}
