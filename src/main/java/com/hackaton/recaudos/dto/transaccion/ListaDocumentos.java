package com.hackaton.recaudos.dto.transaccion;

import java.util.List;

public class ListaDocumentos {

    private List<Documento> documento;

    public List<Documento> getDocumento() {
        return documento;
    }

    public void setDocumento(List<Documento> documento) {
        this.documento = documento;
    }
}
