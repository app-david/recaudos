package com.hackaton.recaudos.dto.transaccion;

public class RecaudosRq {

    private Cabecera cabecera;
    private Detalle detalle;

    public Cabecera getCabecera() {
        return cabecera;
    }

    public void setCabecera(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    public Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Detalle detalle) {
        this.detalle = detalle;
    }
}
