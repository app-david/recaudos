package com.hackaton.recaudos.dto.transaccion;

public class ConsultaDeuda {

    private RecaudosRq recaudosRq;

    public RecaudosRq getRecaudosRq() {
        return recaudosRq;
    }

    public void setRecaudosRq(RecaudosRq recaudosRq) {
        this.recaudosRq = recaudosRq;
    }
}
