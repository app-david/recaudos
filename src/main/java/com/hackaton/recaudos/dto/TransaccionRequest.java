package com.hackaton.recaudos.dto;

import com.hackaton.recaudos.dto.transaccion.ConsultaDeuda;

public class TransaccionRequest {

    private ConsultaDeuda consultaDeuda;

    public ConsultaDeuda getConsultaDeuda() {
        return consultaDeuda;
    }

    public void setConsultaDeuda(ConsultaDeuda consultaDeuda) {
        this.consultaDeuda = consultaDeuda;
    }
}
