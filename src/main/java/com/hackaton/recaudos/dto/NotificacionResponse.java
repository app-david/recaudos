package com.hackaton.recaudos.dto;

import com.hackaton.recaudos.dto.notificacion.NotificacionPago;
import com.hackaton.recaudos.dto.transaccion.ConsultaDeudaResponse;

public class NotificacionResponse {

    private ConsultaDeudaResponse notificarPagoResponse;

    public ConsultaDeudaResponse getNotificarPagoResponse() {
        return notificarPagoResponse;
    }

    public void setNotificarPagoResponse(ConsultaDeudaResponse notificarPagoResponse) {
        this.notificarPagoResponse = notificarPagoResponse;
    }
}
