package com.hackaton.recaudos.controller;

import com.hackaton.recaudos.dto.Convenio;
import com.hackaton.recaudos.excepcion.NegocioException;
import com.hackaton.recaudos.services.ConvenioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recaudos/v1/convenios")
public class ConvenioController {

    @Autowired
    private ConvenioService convenioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void guardarConvenio(@RequestBody Convenio convenio){
        convenioService.crearConvenio(convenio);
    }

    @PutMapping("/{codigo}")
    public ResponseEntity updateConvenio(@PathVariable String codigo, @RequestBody Convenio convenio) {
        Convenio conBD = convenioService.consultarConvenio(codigo);
        if (conBD == null) {
            return new ResponseEntity<>("Convenio no encontrado.", HttpStatus.NOT_FOUND);
        }
        convenio.setCodigo(codigo);
        convenioService.actualizarConvenio(convenio);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.NO_CONTENT);
    }

    @GetMapping
    public List<Convenio> listarConvenios(){
        return convenioService.listarConvenio();
    }


}
