package com.hackaton.recaudos.controller;

import com.hackaton.recaudos.dto.NotificacionResponse;
import com.hackaton.recaudos.dto.notificacion.Notificacion;
import com.hackaton.recaudos.dto.TransaccionResponse;
import com.hackaton.recaudos.excepcion.NegocioException;
import com.hackaton.recaudos.services.RecaudosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/recaudos/v1")
public class RecaudosController {

    @Autowired
    private RecaudosService recaudosService;

    @GetMapping("/deuda")
    public ResponseEntity consultarDeuda(@RequestParam String convenio, @RequestParam String referencia) {
        try{
            TransaccionResponse respuesta = recaudosService.consultarDeuda(convenio, referencia);
            return  ResponseEntity.status(HttpStatus.OK).body(respuesta);
        } catch (NegocioException e){
            return  ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMensaje());
        }
    }

    @PostMapping("/notificarPago")
    public ResponseEntity notificarPago(@RequestBody Notificacion notificacion) {
        try{
            NotificacionResponse respuesta = recaudosService.notificarPago(notificacion);
            return  ResponseEntity.status(HttpStatus.OK).body(respuesta);
        } catch (NegocioException e){
            return  ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMensaje());
        }
    }

}
