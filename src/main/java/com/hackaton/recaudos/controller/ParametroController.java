package com.hackaton.recaudos.controller;

import com.hackaton.recaudos.dto.Convenio;
import com.hackaton.recaudos.dto.Parametro;
import com.hackaton.recaudos.services.ConvenioService;
import com.hackaton.recaudos.services.ParametroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recaudos/v1/parametros")
public class ParametroController {

    @Autowired
    private ParametroService parametroService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void guardarConvenio(@RequestBody Parametro parametro){
        parametroService.crearParametro(parametro);
    }

    @GetMapping
    public List<Parametro> listarParametro(){
        return parametroService.listarParametros();
    }
}
