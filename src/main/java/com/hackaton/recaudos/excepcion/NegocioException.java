package com.hackaton.recaudos.excepcion;

public class NegocioException extends Exception{

    private String codigo;
    private String mensaje;

    public NegocioException(String codigo, String mensaje){
        super();
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    public NegocioException(String mensaje){
        this.mensaje = mensaje;
    }


    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
