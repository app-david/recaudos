package com.hackaton.recaudos.services.impl;

import com.hackaton.recaudos.dao.MovimientoDAO;
import com.hackaton.recaudos.dto.Movimiento;
import com.hackaton.recaudos.services.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovimientoServiceImpl implements MovimientoService {

    @Autowired
    private MovimientoDAO movimientoDAO;

    @Override
    public void crearMovimiento(Movimiento movimiento) {
        movimientoDAO.save(movimiento);
    }

    @Override
    public Movimiento consultarMovimiento(int numeroOperacion) {
        return null;
    }
}
