package com.hackaton.recaudos.services.impl;

import com.hackaton.recaudos.dao.ConvenioDAO;
import com.hackaton.recaudos.dto.Convenio;
import com.hackaton.recaudos.excepcion.NegocioException;
import com.hackaton.recaudos.services.ConvenioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConvenioServiceImpl implements ConvenioService {

    @Autowired
    private ConvenioDAO convenioDAO;

    @Override
    public void crearConvenio(Convenio convenio) {
        convenioDAO.save(convenio);
    }

    @Override
    public void actualizarConvenio(Convenio convenio){
        convenioDAO.save(convenio);
    }

    @Override
    public Convenio consultarConvenio(String codigo) {
        Optional<Convenio> respuesta =  convenioDAO.findById(codigo);
        return respuesta.isPresent() ? respuesta.get() : null;
    }

    @Override
    public List<Convenio> listarConvenio() {
        return convenioDAO.findAll();
    }
}
