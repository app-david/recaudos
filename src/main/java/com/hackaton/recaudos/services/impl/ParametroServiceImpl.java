package com.hackaton.recaudos.services.impl;

import com.hackaton.recaudos.dao.ParametroDAO;
import com.hackaton.recaudos.dto.Parametro;
import com.hackaton.recaudos.services.ParametroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParametroServiceImpl implements ParametroService {

    @Autowired
    private ParametroDAO parametroDAO;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void crearParametro(Parametro parametro) {
        parametroDAO.save(parametro);
    }

    @Override
    public List<Parametro> listarParametros() {
        return parametroDAO.findAll();
    }

    @Override
    public Parametro buscarParametro(String convenio, String operacion) {
        Criteria criteria = Criteria.where("convenio").is(convenio).andOperator(Criteria.where("operacion").is(operacion));;
        Query query = new Query();
        query.addCriteria(criteria);
        return mongoTemplate.findOne(query, Parametro.class);
    }
}
