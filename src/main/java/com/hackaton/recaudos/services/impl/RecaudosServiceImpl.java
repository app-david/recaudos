package com.hackaton.recaudos.services.impl;

import com.hackaton.recaudos.dto.*;
import com.hackaton.recaudos.dto.notificacion.Notificacion;
import com.hackaton.recaudos.dto.notificacion.NotificacionPago;
import com.hackaton.recaudos.dto.transaccion.*;
import com.hackaton.recaudos.excepcion.NegocioException;
import com.hackaton.recaudos.services.ConvenioService;
import com.hackaton.recaudos.services.ParametroService;
import com.hackaton.recaudos.services.RecaudosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import static com.hackaton.recaudos.util.Constantes.*;

@Service
public class RecaudosServiceImpl implements RecaudosService {

    @Autowired
    private ConvenioService convenioService;

    @Autowired
    private ParametroService parametroService;

    AtomicInteger generarOperacion = new AtomicInteger(1);

    @Override
    public TransaccionResponse consultarDeuda(String codigoConvenio, String referencia) throws NegocioException {
        Convenio convenio = convenioService.consultarConvenio(codigoConvenio);
        if(convenio == null) {
            throw new NegocioException("Convenio " + codigoConvenio + " no existe.");
        }
        if(convenio.getEstado().equals(ESTADO_SUSPENDIDO)){
            throw new NegocioException("Convenio " + codigoConvenio + " está suspendido.");
        }

        //INVOCAR AL SERVICIO EXTERNO QUE CORRESPONSA
        TransaccionRequest request = armarBodyConsulta(codigoConvenio, referencia);

        //Se obtiene la url del servicio de la empresa para consultar la deuda
        Parametro paramURL = parametroService.buscarParametro(convenio.getCodigo(), OPERACION_CONSULTA);
        if(paramURL == null){
            throw new NegocioException("No existe la URL del servicio asociado al convenio " + convenio.getCodigo() +
                    ". Puede registrarlo en /recaudos/v1/parametros");
        }
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<TransaccionResponse> response = restTemplate.postForEntity(paramURL.getUrl(), request, TransaccionResponse.class);
        return response.getBody();
    }

    @Override
    public NotificacionResponse notificarPago(Notificacion notificacion) throws NegocioException {
        //INVOCAR AL SERVICIO EXTERNO QUE CORRESPONSA
        NotificacionRequest request = armarBodyNotificacion(notificacion.getCodigoConvenio(), notificacion.getTransaccionPago());

        //Se obtiene la url del servicio de la empresa para notificar el pago
        Parametro paramURL = parametroService.buscarParametro(notificacion.getCodigoConvenio(), OPERACION_PAGO);
        if(paramURL == null){
            throw new NegocioException("No existe la URL del servicio asociado al convenio " + notificacion.getCodigoConvenio() +
                    ". Puede registrarlo en /recaudos/v1/parametros");
        }
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<NotificacionResponse> response = restTemplate.postForEntity(paramURL.getUrl(), request, NotificacionResponse.class);
        return response.getBody();
    }

    private String dateToString(Date fecha){
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        return dateFormat.format(fecha);
    }

    private String horaToString(Date hora){
        DateFormat dateFormat = new SimpleDateFormat("hhmmss");
        return dateFormat.format(hora);
    }

    private TransaccionRequest armarBodyConsulta(String codigoConvenio, String referencia){

        Date hoy = new Date();

        Operacion operacion = new Operacion();
        operacion.setCanalOperacion(CANAL_EXTERNO);
        operacion.setCodigoOperacion(OPERACION_CONSULTA);
        operacion.setCodigoBanco(CODIGO_BANCO);
        operacion.setCodigoConvenio(codigoConvenio);
        operacion.setCodigoOficina(CODIGO_API);
        //Simulacion de generacion de Operacion (LO REALIZA HOST)
        operacion.setNumeroOperacion(this.generarOperacionHOST());

        operacion.setFechaOperacion(dateToString(hoy));
        operacion.setHoraOperacion(horaToString(hoy));

        Transaccion transaccion = new Transaccion();
        transaccion.setNumeroReferenciaDeuda(referencia);

        Detalle detalle = new Detalle();
        detalle.setTransaccion(transaccion);

        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(operacion);

        RecaudosRq recaudoRq = new RecaudosRq();
        recaudoRq.setCabecera(cabecera);
        recaudoRq.setDetalle(detalle);

        ConsultaDeuda consulta = new ConsultaDeuda();
        consulta.setRecaudosRq(recaudoRq);

        TransaccionRequest treq = new TransaccionRequest();
        treq.setConsultaDeuda(consulta);
        return treq;
    }

    private int generarOperacionHOST(){
        return generarOperacion.getAndIncrement();
    }

    public NotificacionRequest armarBodyNotificacion(String codigoConvenio, Transaccion transaccionPago){
        Date hoy = new Date();

        Operacion operacion = new Operacion();
        operacion.setCanalOperacion(CANAL_EXTERNO);
        operacion.setCodigoOperacion(OPERACION_CONSULTA);
        operacion.setCodigoBanco(CODIGO_BANCO);
        operacion.setCodigoConvenio(codigoConvenio);
        operacion.setCodigoOficina(CODIGO_API);
        //Simulacion de generacion de Operacion (LO REALIZA HOST)
        operacion.setNumeroOperacion(this.generarOperacionHOST());

        operacion.setFechaOperacion(dateToString(hoy));
        operacion.setHoraOperacion(horaToString(hoy));

        Detalle detalle = new Detalle();
        detalle.setTransaccion(transaccionPago);

        Cabecera cabecera = new Cabecera();
        cabecera.setOperacion(operacion);

        RecaudosRq recaudoRq = new RecaudosRq();
        recaudoRq.setCabecera(cabecera);
        recaudoRq.setDetalle(detalle);

        NotificacionPago notificacionPago = new NotificacionPago();
        notificacionPago.setRecaudosRq(recaudoRq);

        NotificacionRequest request = new NotificacionRequest();
        request.setNotificarPago(notificacionPago);
        return request;
    }
}
