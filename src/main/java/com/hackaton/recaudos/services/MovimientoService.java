package com.hackaton.recaudos.services;

import com.hackaton.recaudos.dto.Movimiento;

public interface MovimientoService {

    public void crearMovimiento(Movimiento movimiento);
    public Movimiento consultarMovimiento(int numeroOperacion);

}
