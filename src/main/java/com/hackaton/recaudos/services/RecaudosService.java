package com.hackaton.recaudos.services;

import com.hackaton.recaudos.dto.NotificacionResponse;
import com.hackaton.recaudos.dto.notificacion.Notificacion;
import com.hackaton.recaudos.dto.TransaccionResponse;
import com.hackaton.recaudos.excepcion.NegocioException;

public interface RecaudosService {

    public TransaccionResponse consultarDeuda(String convenio, String referencia) throws NegocioException;
    public NotificacionResponse notificarPago(Notificacion notificacion) throws NegocioException;
}
