package com.hackaton.recaudos.services;

import com.hackaton.recaudos.dto.Parametro;

import java.util.List;

public interface ParametroService {

    public  void crearParametro(Parametro parametro);
    public List<Parametro> listarParametros();
    public Parametro buscarParametro(String convenio, String operacion);
}
