package com.hackaton.recaudos.services;

import com.hackaton.recaudos.dto.Convenio;
import com.hackaton.recaudos.dto.Parametro;
import com.hackaton.recaudos.excepcion.NegocioException;

import java.util.List;

public interface ConvenioService {

    public void crearConvenio(Convenio convenio);
    public void actualizarConvenio(Convenio convenio);
    public Convenio consultarConvenio(String codigo);
    public List<Convenio> listarConvenio();
}
