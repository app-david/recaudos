package com.hackaton.recaudos.dao;

import com.hackaton.recaudos.dto.Movimiento;
import com.hackaton.recaudos.dto.Parametro;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParametroDAO extends MongoRepository<Parametro, String> {

}
