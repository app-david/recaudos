package com.hackaton.recaudos.dao;

import com.hackaton.recaudos.dto.Convenio;
import com.hackaton.recaudos.dto.Movimiento;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovimientoDAO extends MongoRepository<Movimiento, String> {

}
