package com.hackaton.recaudos.dao;

import com.hackaton.recaudos.dto.Convenio;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConvenioDAO extends MongoRepository<Convenio, String> {

}
