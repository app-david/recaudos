package com.hackaton.recaudos.util;

public class Constantes {
    public static final String ESTADO_ACTIVO = "N";
    public static final String ESTADO_SUSPENDIDO = "S";

    public static final String OPERACION_CONSULTA = "1010";
    public static final String OPERACION_PAGO = "2010";

    public static final String CANAL_EXTERNO = "EX";
    public static final String CODIGO_BANCO  = "25001";
    public static final String CODIGO_API = "2021";
}
